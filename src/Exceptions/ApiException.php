<?php

namespace Dotcubed\LaravelRadMvc\Exceptions;

use Dotcubed\LaravelRadMvc\Helpers\Response;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

class ApiException extends Exception
{
    /**
     * @param  array<int|string, mixed>  $data
     */
    public static function handleException(\Throwable $exception, mixed $data, ?string $debugMessage = null): JsonResponse
    {
        try {
            $debugMessage = empty($debugMessage) ? 'internal_error - check logs' : $debugMessage;

            //TO-DO once we have authentication in the App this needs to be revisited
            $userUuid = null;
            //$userUuid = request()->user() !== null ? request()->user()->uuid : 'not_logged';
            $requestId = uniqid();

            $routeParameters = [];
            $requestParameters = [];
            if (request()->route()) {
                $routeParameters = request()->route()->parameters();
                $requestParameters = request()->all();
            }

            foreach ($requestParameters as &$requestParameterValue) {
                if (is_object($requestParameterValue)) {
                    $requestParameterValue = '<object>';
                }
            }

            $exceptionCode = self::getExceptionCode($exception);

            $stackTrace = $exception->getTrace();
            $formattedTrace = [];

            foreach ($stackTrace as $key => $stackPoint) {
                $trace = "#$key ";
                $trace .= (isset($stackPoint['file']) ? $stackPoint['file'] : 'unknown file');
                $trace .= ' ) @LINE ' . (isset($stackPoint['line']) ? $stackPoint['line'] : 'unknown line') . ': ';
                $trace .= (isset($stackPoint['class']) ? $stackPoint['class'] . '->' : '');
                $trace .= $stackPoint['function'] . '()';
                $formattedTrace[] = $trace;
            }

            $errorData = [
                'timestamp' => microtime(true),
                'routeAction' => Route::currentRouteAction(),
                'currentUrl' => url()->current(),
                'exceptionType' => get_class($exception),
                'user.uuid' => $userUuid,
                'msg' => $exception->getMessage(),
                'debugMessage' => $debugMessage,
                'line' => $exception->getLine(),
                'file' => $exception->getFile(),
                'methodData' => json_encode($data),
                'routeParameters' => json_encode($routeParameters),
                'requestParameters' => json_encode($requestParameters),
                'statusCode' => $exceptionCode,
                'trace' => $formattedTrace,
            ];

            $errorDataString = '';

            foreach ($errorData as $key => $value) {
                if ($errorDataString !== '') {
                    $errorDataString .= ' || ';
                }

                if (is_array($value)) {
                    $value = json_encode($value);
                }
                $errorDataString .= $key . ': ' . $value;
            }

            //error 422 should not be added to the log file since it is a fields request error
            if ($exceptionCode != 422) {
                Log::error($errorDataString);
            }

            if (env('APP_DEBUG', false) === false) {
                $errorData = [];
            }

            switch (get_class($exception)) {
                case "Illuminate\Http\Exceptions\HttpResponseException":
                case "Illuminate\Validation\ValidationException":
                    //exception sent a manual validator error bag used outside of a Form Request
                    if (isset($exception->validator)) {
                        return Response::getJsonResponse('failure', null, null, $exceptionCode, $requestId, $errorData, $exception->validator->getMessageBag());
                    }

                    //the regular Form Request validation error bag is submitted via the data
                    return Response::getJsonResponse('failure', null, null, $exceptionCode, $requestId, $errorData, $data['errors']);
                case "Illuminate\Auth\AuthenticationException":
                case "Laravel\Passport\Exceptions\OAuthServerException":
                    return Response::getJsonResponse('failure', 'unauthorized', null, $exceptionCode, $requestId, $errorData);
                case "Symfony\Component\HttpKernel\Exception\NotFoundHttpException":
                    return Response::getJsonResponse('failure', $exception->getMessage(), null, $exceptionCode, $requestId, $errorData);
                case "Illuminate\Database\Eloquent\ModelNotFoundException":
                    return Response::getJsonResponse('failure', 'Record not found', null, $exceptionCode, $requestId, $errorData);
                case "App\Exceptions\ModelDependenciesException":
                    return Response::getJsonResponse('failure', 'Dependencies found', null, $exceptionCode, $requestId, $errorData, $exception->dependencies);
                case "App\Exceptions\SingleFieldManualException":
                    return Response::getJsonResponse('failure', $exception->message, null, $exceptionCode, $requestId, $errorData);
                default:
                    return Response::getJsonResponse('failure', 'An unexpected error has happened', null, $exceptionCode, $requestId, $errorData);
            }
        } catch (\Exception $e) {
            return Response::getJsonResponse('failure', 'A failure in the error handling has occured - Error code A-0034', [], 500, $requestId, [$e->getMessage()]);
        }
    }

    private static function getExceptionCode(\Throwable $exception): int
    {
        $exceptionCode = 500;
        switch (get_class($exception)) {
            case "Illuminate\Database\Eloquent\ModelNotFoundException":
            case "Symfony\Component\HttpKernel\Exception\NotFoundHttpException":
                $exceptionCode = 404;
                break;
            case "Illuminate\Http\Exceptions\HttpResponseException":
            case "Illuminate\Validation\ValidationException":
            case "App\Exceptions\ModelDependenciesException":
            case "App\Exceptions\SingleFieldManualException":
                $exceptionCode = 422;
                break;
            case "Illuminate\Auth\AuthenticationException":
                $exceptionCode = 401;
                break;
        }

        return $exceptionCode;
    }
}
