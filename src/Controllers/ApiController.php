<?php

namespace Dotcubed\LaravelRadMvc\Controllers;

use Dotcubed\LaravelRadMvc\Exceptions\ApiException;
use Dotcubed\LaravelRadMvc\Helpers\Response;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use RuntimeException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ApiController extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    /**
     * Magic method manipulator for simple methods
     * This will take the routed method and run it against the Model directly (if it exists)
     *
     * It assumes that the first parameters is always the UUID that will fetch the Record and
     * then run the method against that specific record
     *
     * @param  array<mixed, mixed>  $parameters
     */
    public function __call($method, mixed $parameters)
    {
        try {
            Log::info('ApiController - Triggered __call - magic call to an ApiController extended Controller');

            $model = $this->getInferredModelName();

            Log::info('ApiController - checking if method ' . $method . ' exists on Model ' . $model);

            if (! method_exists($model, $method)) {
                Log::info('ApiController - method ' . $method . ' does NOT exist on Model ' . $model . ' - EXITING');

                return \App\Exceptions\ApiException::handleException(
                    new NotFoundHttpException(),
                    [],
                    'Error while triggering a Magic method in the ApiController class.' . chr(10) . 'Model "' . $model . '" does not have a "' . $method . '" method'
                );
            }

            Log::info('ApiController - using model ' . $model . ' method ' . $method);

            if (! is_array($parameters)) {
                return Response::failure('Error while triggering a Magic method in the ApiController class.' . chr(10) . 'Parameter lists is not an array');
            }

            $targetObject = $model;
            $targetMethodParameters = [];

            Log::info('ApiController - calling method ' . $method);

            //TO-DO test this more
            //get the querystring and post parameters as well
            if (! empty(request()->all())) {
                $requestParameters = array_merge($targetMethodParameters, request()->all());
                $parameters[] = $requestParameters;
            }
            $formRequestClass = 'App\\Http\\Requests\\' . class_basename($model) . '\\' . ucfirst($method) . 'Request';
            Log::info('ApiController - searching for FormRequest file ' . $formRequestClass);
            if (class_exists($formRequestClass)) {
                Log::info('ApiController - using FormRequest found ' . $formRequestClass);
                $formRequest = app()->make($formRequestClass);
                $formRequest->validateResolved();
            } else {
                Log::info('ApiController - no specific FormRequest file found, proceeding without it.');
            }

            /** @var callable $callable */
            $callable = [$targetObject, $method];

            $methodResult = call_user_func_array(
                $callable,
                $parameters,
            );

            //check if there is a specific Http/Resource for this Model+Method then use it for the response
            $resourceClass = 'App\\Http\\Resources\\' . class_basename($targetObject) . '\\' . ucfirst($method) . 'Resource';
            Log::info('ApiController - searching for Resource file ' . $resourceClass);
            if (class_exists($resourceClass)) {
                Log::info('ApiController - using Resource file found ' . $resourceClass);

                if (get_class($methodResult) != 'Illuminate\Database\Eloquent\Collection') {
                    $resourceOutput = new $resourceClass($methodResult);

                    return Response::success($resourceOutput);
                }

                /** @var callable $callable */
                $callable = [$resourceClass, 'collection'];

                $resourceOutput = call_user_func_array(
                    $callable,
                    [
                        $methodResult,
                    ],
                );

                return Response::success($resourceOutput);
            }

            return Response::success($methodResult);
        } catch (HttpResponseException $e) {
            $response = $e->getResponse();
            $content = $response->getContent();
            $data = is_string($content) ? json_decode($content, true) : null;

            return ApiException::handleException($e, $data);
        } catch (\Exception $e) {
            return ApiException::handleException($e, func_get_args());
        }
    }

    /**
     * Calculates a Model's name based on the Controller that was called in the Request
     */
    public function getInferredModelName(): string
    {
        $route = Route::getCurrentRoute();

        if (! $route) {
            throw new RuntimeException('Request route not reachable in this context');
            //            Response::failure('Request route not reachable in this context');
        }

        $actionName = $route->getActionName();

        if ($actionName === '') {
            throw new RuntimeException('Request route action not reachable in this context');
            //            Response::failure('Request route action not reachable in this context');
        }

        $controllerAction = class_basename($actionName);
        [$controller, $action] = explode('@', $controllerAction);

        Log::info('ApiController - action parsed: controller ' . $controller . ' action ' . $action);

        $controller = substr($controller, 0, -10);

        if (substr($controller, -3) === 'ies') {
            $controller = substr($controller, 0, -3) . 'y';
        }

        if (substr($controller, -1) === 's') {
            $controller = substr($controller, 0, -1);
        }

        $model = '\\App\\Models\\' . $controller;

        if (! class_exists($model)) {
            throw new RuntimeException('Model ' . $model . ' does not exist. Incorrect inferrence from controller name ' . $controller);
        }

        return $model;
    }

    private function logRoutingInformation(): void
    {
        Log::info('ApiController - processing path:  ' . request()->path());
        Log::info('ApiController - routed class/method:  ' . Route::currentRouteAction());
    }
}
