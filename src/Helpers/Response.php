<?php

namespace Dotcubed\LaravelRadMvc\Helpers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class Response
{
    /**
     * @param  array<mixed, mixed>  $errorData
     */
    public static function getJsonResponse(
        string $requestStatus,
        ?string $message,
        mixed $data,
        int $statusCode,
        string $requestId = '',
        array $errorData = [],
        mixed $errorBag = []
    ): JsonResponse {
        if ($requestStatus != 'success' && $requestStatus != 'failure' && $requestStatus != 'warning') {
            self::failureMessage('this endpoint is misconfigured and returing an invalid request status value');
        }

        self::logError($statusCode, $data);
        $requestId = request()->header('X-Request-ID');

        $jsonResponseData = [
            'status' => $requestStatus,
            'statusCode' => $statusCode,
            'data' => $data,
            'requestId' => $requestId,
            'timestamp' => microtime(true),
        ];

        if (! empty($message)) {
            $jsonResponseData['message'] = $message;
        }

        if (! empty($errorBag)) {
            $jsonResponseData['errors'] = $errorBag;
        }

        if (! empty($errorData)) {
            $jsonResponseData['errorData'] = $errorData;
        }

        return response()->json($jsonResponseData, $statusCode, [], JSON_PRETTY_PRINT);
    }

    /**
     * A simple Logging method based on specific HTTP return status codes
     *
     * @param  int  $statusCode  The HTTP statusCode used
     * @param  mixed  $data  The data that should be logged alongside it
     * @return void
     */
    private static function logError($statusCode, $data)
    {
        if ($statusCode === \Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY || $statusCode === \Illuminate\Http\Response::HTTP_I_AM_A_TEAPOT) {
            Log::info($data);
        }
    }

    /**
     * Wrapper to return a simple successful JSON response with data
     *
     * @param  mixed  $data  The data contained in the response
     * @param  mixed  $message  The description message contained in the response
     * @param  mixed  $responseCode  The HTTP response code to be used in the response header
     */
    public static function success($data = null, $message = null, $responseCode = \Illuminate\Http\Response::HTTP_OK): JsonResponse
    {
        return self::getJsonResponse('success', $message, $data, $responseCode);
    }

    /**
     * Wrapper to return a simple successful JSON response message without any data
     */
    public static function successMessage(string $message): JsonResponse
    {
        return self::getJsonResponse('success', $message, [], \Illuminate\Http\Response::HTTP_OK);
    }

    /**
     * Wrapper to return a failed JSON response
     */
    public static function failure(?string $message = null, mixed $data = null, int $responseCode = \Illuminate\Http\Response::HTTP_BAD_REQUEST): JsonResponse
    {
        return self::getJsonResponse('failure', $message, $data, $responseCode);
    }

    /**
     * Wrapper to return a simple failure JSON response message without any data
     */
    public static function failureMessage(string $message): JsonResponse
    {
        return self::getJsonResponse('failure', $message, [], \Illuminate\Http\Response::HTTP_BAD_REQUEST);
    }
}
